﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace Metrics
{
    // The methods in this class extract a plan's dose matrix
    public static class DoseMatrixExtract
    {
        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 1D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>1D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[] GetDoseOneD(Dose dose, Structure structure, double xres, double yres, double zres)
        {
            // If dose is not defined return null
            if (dose == null)
            {
                return null;
            }

            // number of voxels with the desired resolution
            int xcount = (int)Math.Ceiling(dose.XSize * dose.XRes / xres);
            int ycount = (int)Math.Ceiling(dose.YSize * dose.YRes / yres);
            int zcount = (int)Math.Ceiling(dose.ZSize * dose.ZRes / zres);

            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method
            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var doseOneD = new List<double>(); // 1D list 

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * yres +
                                    dose.ZDirection * z * zres;
                    VVector end = start + dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                            }

                            if (!Double.IsNaN(doseProfile[i].Value))
                            {
                                doseOneD.Add(doseProfile[i].Value);
                            }
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseOneD.ToArray();
        }

        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 3D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>3D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[,,] GetDoseThreeD(Dose dose, Structure structure, double xres, double yres, double zres)
        {
            // If dose is not defined return null
            if (dose == null)
            {
                return null;
            }

            // number of voxels with the desired resolution
            int xcount = (int)Math.Ceiling(dose.XSize * dose.XRes / xres);
            int ycount = (int)Math.Ceiling(dose.YSize * dose.YRes / yres);
            int zcount = (int)Math.Ceiling(dose.ZSize * dose.ZRes / zres);

            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseArray = new double[xcount]; // dummy array for profile method
            DoseValue.DoseUnit doseUnit = dose.DoseMax3D.Unit;
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels with desired resolution, default ~1mm

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = dose.Origin +
                                    dose.YDirection * y * yres +
                                    dose.ZDirection * z * zres;
                    VVector end = start + dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = dose.GetDoseProfile(start, end, doseArray);
                            }

                            if (!Double.IsNaN(doseProfile[i].Value))
                            {
                                doseMatrix[i, y, z] = doseProfile[i].Value;
                            }
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        /// <summary>
        /// Method to create binary image of dose matrix to find cold/hot spots. Returns 3D array.
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <param name="cutoff">double</param>
        /// <param name="FindColdVolumes">bool. True: find cold spots, false: find hot spots</param>
        /// <returns>double[,,] of ones and zeros.</returns>
        public static double[,,] CreateBinaryDoseMatrix(Dose dose, Structure structure, double cutoff, double xres, double yres, double zres, bool FindColdVolumes)
        {
            var doseMatrix = GetDoseThreeD(dose, structure, xres, yres, zres);
            var BinaryMatrix = new double[doseMatrix.GetLength(0), doseMatrix.GetLength(1), doseMatrix.GetLength(2)];

            for (int i = 0; i < doseMatrix.GetLength(0); i++)
                for (int j = 0; j < doseMatrix.GetLength(1); j++)
                    for (int k = 0; k < doseMatrix.GetLength(2); k++)
                    {
                        if (FindColdVolumes & doseMatrix[i, j, k] < cutoff & doseMatrix[i, j, k] > 0) // find cold spots
                        {
                            BinaryMatrix[i, j, k] = 1;
                        }

                        else if (!FindColdVolumes & doseMatrix[i, j, k] > cutoff) // find hot spots
                        {
                            BinaryMatrix[i, j, k] = 1;
                        }
                    }
            return BinaryMatrix;
        }

        /// <summary>
        /// Method to create binary image of dose matrix to find cold/hot spots. Returns 3D array. Overload that takes 3D dose array as input.
        /// </summary>
        /// <param name="doseMatrix">double[,,]</param>
        /// <param name="cutoff">double</param>
        /// <param name="FindColdVolumes">bool. True: find cold spots, false: find hot spots</param>
        /// <returns>double[,,] of ones and zeros.</returns>
        public static double[,,] CreateBinaryDoseMatrix(double[,,] doseMatrix, double cutoff, bool FindColdVolumes)
        {
            var BinaryMatrix = new double[doseMatrix.GetLength(0), doseMatrix.GetLength(1), doseMatrix.GetLength(2)];

            for (int i = 0; i < doseMatrix.GetLength(0); i++)
                for (int j = 0; j < doseMatrix.GetLength(1); j++)
                    for (int k = 0; k < doseMatrix.GetLength(2); k++)
                    {
                        if (FindColdVolumes & doseMatrix[i, j, k] < cutoff & doseMatrix[i, j, k] > 0) // find cold spots
                        {
                            BinaryMatrix[i, j, k] = 1;
                        }

                        else if (!FindColdVolumes & doseMatrix[i, j, k] > cutoff) // find hot spots
                        {
                            BinaryMatrix[i, j, k] = 1;
                        }
                    }
            return BinaryMatrix;
        }
    }

}
