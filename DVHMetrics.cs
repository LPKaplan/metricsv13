﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using OxyPlot;

namespace Metrics
{
    // These metrics are based on DVH parameters
    public static class DVHMetrics
    {
        /// <summary>
        /// Method to calculate the conformity number as defined by Paddick et al. or van't Riet et al
        /// </summary>
        /// <param name="plan">The plan to be evaluated. Can be photon, proton, or brachy.</param>
        /// <param name="structure">The structure to be evaluated. Id must contain "PTV" or "CTV".</param>
        /// <param name="doseLevel">The dose level at which to evaluate conformity.</param>
        /// <returns>Double ConformityIndex</returns>
        public static double CalculateConformityIndex(PlanSetup plan, Structure structure, Structure Body, double doseLevel)
        {
            DoseValue CIdose = new DoseValue(doseLevel, DoseValue.DoseUnit.Gy);

            // Structure should be a target
            if (structure.Id.Contains("CTV") || structure.Id.Contains("PTV"))
            {
                DVHData BodyDVH = plan.GetDVHCumulativeData(Body, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                var TVp = plan.GetVolumeAtDose(structure, CIdose, VolumePresentation.AbsoluteCm3);
                var TVtot = structure.Volume;
                double Vp;

                // For imported doses the dose coverage of the body volume can be less than 100%. In this case the GetVolumeAtDose method returns NaN. The VolumeFromDVH method is less accurate, but it works in these caess.
                if (BodyDVH.Coverage < 1.0)
                {
                    Vp = VolumeFromDVH(BodyDVH, doseLevel);
                }
                // if dose coverage is 100%
                else
                {
                    Vp = plan.GetVolumeAtDose(Body, CIdose, VolumePresentation.AbsoluteCm3);
                }

                return (TVtot / TVp) * (Vp / TVp);
            }

            else
            {
                MessageBox.Show("Conformity index is not shown for non-target volumes.");
                return double.NaN;
            }
        }

        /// <summary>
        /// Method that returns the conformity distance index as defined by Wu et al. (2003). Requires evaluation isodose to be rendered in the 3D view.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="doseLevel"></param>
        /// <returns></returns>
        public static double CalculateConformityDistanceIndex(PlanSetup plan, Structure body, Structure refStructure, Structure evalIsodose, double evalDose)
        {
            // Get DVHs
            DVHData dvh = plan.GetDVHCumulativeData(refStructure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
            DVHData bodyDVH = plan.GetDVHCumulativeData(body, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            // initialize variables
            double Vref;
            double VTref;
            
            // For imported doses the dose coverage of the body volume can be less than 100%. In this case the GetVolumeAtDose method returns NaN. The VolumeFromDVH method is less accurate, but it works in these caess.
            if (bodyDVH.Coverage < 1.0)
            {
                Vref = VolumeFromDVH(bodyDVH, evalDose);
            }
            // if dose coverage is 100%
            else
            {
                Vref = plan.GetVolumeAtDose(body, new DoseValue(evalDose, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
            }

            // For imported doses the dose coverage of the body volume can be less than 100%. In this case the GetVolumeAtDose method returns NaN. The VolumeFromDVH method is less accurate, but it works in these caess.
            if (dvh.Coverage < 1.0)
            {
                VTref = VolumeFromDVH(dvh, evalDose);
            }
            // if dose coverage is 100%
            else
            {
                VTref = plan.GetVolumeAtDose(refStructure, new DoseValue(evalDose, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
            }

            double Sref = GetSurfaceArea(refStructure);
            double Seval = GetSurfaceArea(evalIsodose);

            return ((Vref - VTref) + (refStructure.Volume - VTref)) / (0.5 * (Sref + Seval));
        }

        /// <summary>
        /// Calculates the homogeneity index as proposed by the ICRU
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        public static double CalculateHI_ICRU(PlanSetup plan, Structure structure)
        {
            // Get dvh
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.Relative, 0.1);

            // initialize variables
            double D2;
            double D98;
            double D50;

            // if DVH sampling coverage is large enough use GetDoseAtVolume method. Else use DoseFromDVH
            if (dvh.Coverage < 1.0)
            {
                D2 = DoseFromDVH(dvh, 2);
                D98 = DoseFromDVH(dvh, 98);
                D50 = DoseFromDVH(dvh, 50);
            }
            else
            {
                D2 = plan.GetDoseAtVolume(structure, 2, VolumePresentation.Relative, DoseValuePresentation.Absolute).Dose;
                D98 = plan.GetDoseAtVolume(structure, 98, VolumePresentation.Relative, DoseValuePresentation.Absolute).Dose;
                D50 = plan.GetDoseAtVolume(structure, 50, VolumePresentation.Relative, DoseValuePresentation.Absolute).Dose;
            }

            return (D2 - D98) / D50;
        }

        /// <summary>
        /// Returns the gradient index defined as the effective distance between two isodose volumes.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="refdose"></param>
        /// <param name="tempdose"></param>
        /// <returns></returns>
        public static double CalculateGradientEffRadDiff(PlanSetup plan, Structure Body, double referenceDose, double evalDose)
        {
            double VX;
            double Vref;

            DVHData dvh = plan.GetDVHCumulativeData(Body, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            // For imported doses the dose coverage of the body volume can be less than 100%. In this case the GetVolumeAtDose method returns NaN. The VolumeFromDVH method is less accurate, but it works in these caess.
            if (dvh.Coverage < 1.0)
            {
                VX = VolumeFromDVH(dvh, evalDose);
                Vref = VolumeFromDVH(dvh, referenceDose);
            }
            // if dose coverage is 100%
            else
            {
                VX = plan.GetVolumeAtDose(Body, new DoseValue(evalDose, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
                Vref = plan.GetVolumeAtDose(Body, new DoseValue(referenceDose, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
            }

            double effRadX = Math.Pow((3 * VX) / (4 * Math.PI), 1.0 / 3.0);
            double effRadRef = Math.Pow((3 * Vref) / (4 * Math.PI), 1.0 / 3.0);

            return effRadX - effRadRef;
        }

        /// <summary>
        /// Returns GCI as proposed by Sung and Choi
        /// </summary>
        /// <param name="referenceStructure"></param>
        /// <param name="isodose"></param>
        /// <returns></returns>
        public static double CalculateDoseGradientIndex(Structure referenceStructure, Structure isodose)
        {
            double V2 = isodose.Volume;
            double V1 = referenceStructure.Volume;
            double S2 = GetSurfaceArea(isodose);
            double S1 = GetSurfaceArea(referenceStructure);

            return (V2 - V1) / (0.5 * (S2 + S1));
        }

        /// <summary>
        /// Returns GCI as proposed by Sung and Choi. Overload in evalIsodose is an Isodose object
        /// </summary>
        /// <param name="referenceStructure"></param>
        /// <param name="isodose"></param>
        /// <returns></returns>
        public static double CalculateDoseGradientIndex(Structure referenceStructure, Isodose isodose)
        {
            double V2 = GetIsodoseVolume(isodose);
            double V1 = referenceStructure.Volume;
            double S2 = GetSurfaceArea(isodose);
            double S1 = GetSurfaceArea(referenceStructure);

            return (V2 - V1) / (0.5 * (S2 + S1));
        }

        /// <summary>
        /// Returns GCI as proposed by Sung and Choi. Overload if both refIsodose and evalIsodose are Isodose objects
        /// </summary>
        /// <param name="referenceStructure"></param>
        /// <param name="isodose"></param>
        /// <returns></returns>
        public static double CalculateDoseGradientIndex(Isodose referenceStructure, Isodose isodose)
        {
            double V2 = GetIsodoseVolume(isodose);
            double V1 = GetIsodoseVolume(referenceStructure);
            double S2 = GetSurfaceArea(isodose);
            double S1 = GetSurfaceArea(referenceStructure);

            return (V2 - V1) / (0.5 * (S2 + S1));
        }

        /// <summary>
        /// Dose gradient index for gradient curve calculated based on isodoses rendered in the Eclipse UI. Differential curve data.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static List<DataPoint> GetDifferentialGradientCurveDataIsodose(PlanSetup plan)
        {
            List<DataPoint> curve = new List<DataPoint>();

            // If isodose objects are not rendered in the GUI they can't be accessed in ESAPI13....
            if (plan.Dose.Isodoses.Count() < 2)
            {
                MessageBox.Show("Please render two or more isodoses in 3D in the GUI!");
                return curve;
            }

            for (int i = 0; i < plan.Dose.Isodoses.Count() - 1; i++)
            {
                double V1 = GetIsodoseVolume(plan.Dose.Isodoses.ElementAt(i));
                double V2 = GetIsodoseVolume(plan.Dose.Isodoses.ElementAt(i + 1));
                double S1 = GetSurfaceArea(plan.Dose.Isodoses.ElementAt(i));
                double S2 = GetSurfaceArea(plan.Dose.Isodoses.ElementAt(i + 1));

                double dist = (V2 - V1) / (0.5 * (S2 + S1));

                curve.Add(new DataPoint(plan.Dose.Isodoses.ElementAt(i).Level.Dose, dist));
            }

            return curve;
        }

        /// <summary>
        /// Dose gradient index for gradient curve calculated based on isodoses rendered in the Eclipse UI. Cumulative curve data.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static List<DataPoint> GetCumulativeGradientCurveDataIsodose(PlanSetup plan)
        {
            List<DataPoint> curve = new List<DataPoint>();
            List<DataPoint> diffList = new List<DataPoint> { new DataPoint(100, 0) };

            // If isodose objects are not rendered in the GUI they can't be accessed in ESAPI13....
            if (plan.Dose.Isodoses.Count() < 2)
            {
                MessageBox.Show("Please render two or more isodoses in 3D in the GUI!");
                return curve;
            }

            plan.Dose.Isodoses.ToList().Sort(delegate (Isodose x, Isodose y)
            {
                return x.Level.Dose.CompareTo(y.Level.Dose);
            });

            for (int i = plan.Dose.Isodoses.Count() - 1; i > 0; i--)
            {
                double V2 = GetIsodoseVolume(plan.Dose.Isodoses.ElementAt(i));
                double V1 = GetIsodoseVolume(plan.Dose.Isodoses.ElementAt(i - 1));
                double S2 = GetSurfaceArea(plan.Dose.Isodoses.ElementAt(i));
                double S1 = GetSurfaceArea(plan.Dose.Isodoses.ElementAt(i - 1));

                double dgi = (V2 - V1) / (0.5 * (S2 + S1));
                diffList.Add(new DataPoint(plan.Dose.Isodoses.ElementAt(i).Level.Dose, dgi));
            }

            for (int i = plan.Dose.Isodoses.Count() - 1; i > 0; i--)
            {
                double cDGI = diffList.Where(point => point.X > plan.Dose.Isodoses.ElementAt(i).Level.Dose).Sum(point => point.Y);
                curve.Add(new DataPoint(plan.Dose.Isodoses.ElementAt(i - 1).Level.Dose, cDGI));
            }

            return curve;
        }

        /// <summary>
        /// Dose gradient index for gradient curve calculated based on isodose structures which must be generated in the Eclipse UI prior to running this script.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static List<DataPoint> GetDifferentialGradientCurveDataStructure(PlanSetup plan)
        {
            // initialize retrun variable and list to contain all dose structures
            List<DataPoint> curve = new List<DataPoint>();
            List<Structure> doseStructures = new List<Structure>();

            // If isodose objects are not rendered in the GUI they can't be accessed in ESAPI13....
            if (plan.StructureSet.Structures.Count(s => s.Id.Contains("Dose")) < 2)
            {
                MessageBox.Show("Please generate two or more isodose structures in the UI!");
                return curve;
            }

            // Collect all isodose structures in a list
            foreach (var structure in plan.StructureSet.Structures)
            {
                if (structure.Id.Contains("Dose"))
                {
                    doseStructures.Add(structure);
                }
            }

            // Calculate DGI and collect in list
            for (int i = 0; i < doseStructures.Count() - 1; i++)
            {
                double V1 = doseStructures.ElementAt(i).Volume;
                double V2 = doseStructures.ElementAt(i + 1).Volume;
                double S1 = GetSurfaceArea(doseStructures.ElementAt(i));
                double S2 = GetSurfaceArea(doseStructures.ElementAt(i + 1));

                double dist = (V2 - V1) / (0.5 * (S2 + S1));

                // Find out if isodose structure is in absolute or relative units
                string id = doseStructures.ElementAt(i).Id;

                if (id[id.Length - 2] == 'y' & id.Length == 10) // if dose is given in Gy and is a 1-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6])), dist));
                }
                else if (id[id.Length - 2] == 'y' & id.Length == 11) // if dose is given in Gy and is a 2-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7])), dist));
                }
                else if (id[id.Length - 2] == 'y' & id.Length == 12) // if dose is given in Gy and is a 3-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7]) + Convert.ToString(id[8])), dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 9) // if dose is given in % and is a 1-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 10) // if dose is given in % and is a 2-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 11) // if dose is given in % and is a 3-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7]) + Convert.ToString(id[8])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
            }

            return curve;
        }

        /// <summary>
        /// Dose gradient index calculated based on isodose structures which must be generated in the Eclipse UI prior to running this script. Reference structure can be either an isodose structure (e.g. 100%) or an ROI (e.g. a target).
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public static List<DataPoint> GetCumulativeGradientCurveData(PlanSetup plan, Structure referenceStructure)
        {
            // initialize lists
            List<DataPoint> curve = new List<DataPoint>();
            List<Structure> doseStructures = new List<Structure>();

            // Script method ConvertIsodoseToStructure is accessible only in ESAPI15 and later...
            if (plan.StructureSet.Structures.Count(s => s.Id.Contains("Dose")) < 2)
            {
                MessageBox.Show("Please generate two or more isodose structures in the UI!");
                return curve;
            }

            // Collect all isodose structures in a list
            foreach (var structure in plan.StructureSet.Structures)
            {
                if (structure.Id.Contains("Dose"))
                {
                    doseStructures.Add(structure);
                }
            }

            for (int i = doseStructures.Count() - 1; i > 0; i--)
            {
                double V2 = doseStructures.ElementAt(i).Volume;
                double V1 = referenceStructure.Volume;
                double S2 = GetSurfaceArea(doseStructures.ElementAt(i));
                double S1 = GetSurfaceArea(referenceStructure);

                double dist = (V2 - V1) / (0.5 * (S2 + S1));

                // Find out if isodose structure is in absolute or relative units
                string id = doseStructures.ElementAt(i).Id;

                if (id[id.Length - 2] == 'y' & id.Length == 10) // if dose is given in Gy and is a 1-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6])), dist));
                }
                else if (id[id.Length - 2] == 'y' & id.Length == 11) // if dose is given in Gy and is a 2-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7])), dist));
                }
                else if (id[id.Length - 2] == 'y' & id.Length == 12) // if dose is given in Gy and is a 3-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7]) + Convert.ToString(id[8])), dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 9) // if dose is given in % and is a 1-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 10) // if dose is given in % and is a 2-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
                else if (id[id.Length - 2] == '%' & id.Length == 11) // if dose is given in % and is a 3-digit value
                {
                    curve.Add(new DataPoint(Convert.ToDouble(Convert.ToString(id[6]) + Convert.ToString(id[7]) + Convert.ToString(id[8])) * plan.TotalPrescribedDose.Dose / 100, dist));
                }
            }

            return curve;
        }



        #region Helper methods
        /// <summary>
        /// Interpolates dvh curve to return dose for a given volume. Volume given in the same units as used in the DVHData.
        /// </summary>
        /// <param name="dvh"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double DoseFromDVH(DVHData dvh, double value)
        {
            double ans = 0;
            List<DVHPoint> curve = dvh.CurveData.ToList();
            if (curve.Any(i => i.Volume <= value))
            {
                int index = curve.IndexOf(curve.First(i => i.Volume <= value));
                if (index > 0)
                {
                    ans = curve[index - 1].DoseValue.Dose + (value - curve[index - 1].Volume) *
                            (curve[index].DoseValue.Dose - curve[index - 1].DoseValue.Dose) /
                            (curve[index].Volume - curve[index - 1].Volume);
                }
                else
                {
                    ans = curve[index].DoseValue.Dose;
                }
            }


            return ans;
        }

        /// <summary>
        /// Interpolates dvh curve to return volume for a given dose
        /// If the requested dose is higher than the last item in the DVH curve then none of the contour
        /// receives this dose and 0 is returned.
        /// </summary>
        /// <param name="dvh"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double VolumeFromDVH(DVHData dvh, double value)
        {
            double ans;
            List<DVHPoint> curve = dvh.CurveData.ToList();
            if (value <= curve.Last().DoseValue.Dose)
            {
                int index = curve.IndexOf(curve.First(i => i.DoseValue.Dose >= value));
                ans = curve[index - 1].Volume + (value - curve[index - 1].DoseValue.Dose) *
                (curve[index].Volume - curve[index - 1].Volume) /
                (curve[index].DoseValue.Dose - curve[index - 1].DoseValue.Dose);
            }
            else
            {
                ans = 0;
            }
            return ans;
        }

        /// <summary>
        /// Method that returns the surface area of a structure
        /// </summary>
        /// <param name="isodose"></param>
        /// <returns></returns>
        public static double GetSurfaceArea(Structure structure)
        {
            var points = structure.MeshGeometry.Positions;
            var triangleIndices = structure.MeshGeometry.TriangleIndices;
            double area = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                Point3D a = points.ElementAt(triangleIndices.ElementAt(i * 3));
                Point3D b = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                Point3D c = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));

                Vector3D ab = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
                Vector3D ac = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);

                double abDOTac = (ab.X * ac.X) + (ab.Y * ac.Y) + (ab.Z * ac.Z);

                if (ab.Length > 0 && ac.Length > 0)
                {
                    var val = 0.5 * ab.Length * ac.Length * Math.Sqrt(1 - Math.Pow(abDOTac / (ab.Length * ac.Length), 2));
                    if (!double.IsNaN(val))
                        area += val;
                }
            }
            return area / 100;
        }

        public static double GetSurfaceArea(Isodose isodose)
        {
            var points = isodose.MeshGeometry.Positions;
            var triangleIndices = isodose.MeshGeometry.TriangleIndices;
            double area = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                Point3D a = points.ElementAt(triangleIndices.ElementAt(i * 3));
                Point3D b = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                Point3D c = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));

                Vector3D ab = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
                Vector3D ac = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);

                double abDOTac = (ab.X * ac.X) + (ab.Y * ac.Y) + (ab.Z * ac.Z);

                if (ab.Length > 0 && ac.Length > 0)
                {
                    var val = 0.5 * ab.Length * ac.Length * Math.Sqrt(1 - Math.Pow(abDOTac / (ab.Length * ac.Length), 2));
                    if (!double.IsNaN(val))
                        area += val;
                }
            }
            return area / 100;
        }

        private static double GetIsodoseVolume(Isodose isodose)
        {
            Point3D v1;
            Point3D v2;
            Point3D v3;
            var points = isodose.MeshGeometry.Positions;
            var triangleIndices = isodose.MeshGeometry.TriangleIndices;
            double vol = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                v1 = points.ElementAt(triangleIndices.ElementAt(i * 3));
                v2 = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                v3 = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));
                vol += (((v2.Y - v1.Y) * (v3.Z - v1.Z) - (v2.Z - v1.Z) * (v3.Y - v1.Y)) * (v1.X + v2.X + v3.X)) / 6;
            }
            return vol / 1000; // cmm to cc
        }
        #endregion
    }
}
